# sampleproject
This is a simple Spring boot rest api application. The project includes CRUD methods and simple logic for posts and comments.

## What you'll need
- Java 11
- Docker
- MySQL with database (while run using gradle only)

## To start
To start this application, clone this repo (https://gitlab.com/tposiadala/sampleproject.git)

## Run using gradle (needs to setup MySQL database first and adjust application.properties)

Build using gradle from project location:
```
./gradlew build -x test
```
Run application:
```
./build/libs/sampleproject-0.0.1-SNAPSHOT.jar
```
or
```
java -jar build/libs/sampleproject-0.0.1-SNAPSHOT.jar
```

## To run as a docker container (recommended)

Build image and run container:
```
docker-compose up
```

## Check using swagger:
```
http://localhost:8084/swagger-ui.html
```

Use Login controller with this credentials:

```
{
  "password": "test",
  "username": "test"
}
```

Copy authorization header e.g.:
```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjE4MjUzNDg2fQ.RQH7nO3GOFpatVfiEgWnKvh80aOJDSkoljLBKI3VKU0
```
Click "Authorize" button on top right, paste the header and authorize. Now you can ow you can use API. 

## Adminer
You can also use Adminer for managing content in MySQL database:
```
http://localhost:8082/
```
user: root  
password: example  
database: forum  
