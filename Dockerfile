FROM gradle:jdk11 AS build

COPY --chown=gradle:gradle . /home/gradle/src

WORKDIR /home/gradle/src

RUN gradle clean build -x test

FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/*.jar /app/sampleproject-application.jar

ENTRYPOINT ["java","-jar","/app/sampleproject-application.jar"]